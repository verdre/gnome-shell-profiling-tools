Some profiling tools for GNOME shell

Example for running a test: "BRANCH=clutter-content bin/run_test.sh tests/profile-30-glxgears-in-overview 10"

This runs the test in tests/profile-30-glxgears-in-overview ten times and uses the text given with BRANCH to describe the tests. There's also the DESC environment variable you can set to further describe the test you're doing (if you want to test different configurations (ie. x11+wayland) with the same branch), and DRY_RUN to run the test without sysprof.

---

The environment for testing is setup by bin/setup_env.sh, it locks your intel cpu to 50 percent performance and disables turbo boost.

---

Getting fancy graphs: "bin/createcharts.py -m Compositor:Paint"

This reads the syscap files from out/, creates graphs for the specified Sysprof mark and opens them in your browser.

---

A lot of commands have to be repeatedly executed as root, so it's normal that the pkexec dialog pops up very often and might make sense to temporarily disable asking for the root password (especially if that's needed during a test).

Warping the pointer currently doesn't work (you'd have to export the function in Mutter), I'm planning to use ponytail [1] by Olivier Fourdan for this in the future (probably rewriting everything except the createcharts.py script).

Also the tests involving bin/play.sh won't work right now since this needs a patched libinput-tools.

Finally, sorry for including a binary (bin/extract_data) with this. This binary is used to parse the syscap files and generate json data from them. The source code for it is on another computer I don't have access to right now :/

[1] https://gitlab.gnome.org/ofourdan/gnome-ponytail-daemon

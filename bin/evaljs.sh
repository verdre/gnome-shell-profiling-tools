#!/bin/sh

finish () {
  if [ $blockpid != 0 ]; then
    echo "Killing pipe with pid $blockpid"
    kill $blockpid
  fi
}

if [ "$1" == "-b" -o "$1" == "--blocking" ]; then
  # Seems like storing the pid of grep and killing grep is enough
  # because journalctl exits by itself if the pipe breaks.
  journalctl -f -n0 -q | grep -q "TEST_FINISHED" &
  blockpid=$!

  trap finish EXIT

  # For some reason setting up the waiting stuff seems to take longer than
  # the shell executing the stuff we send to it, wait a second to make sure
  # we're listening.
  sleep 0.5
fi

case "$1" in
  -f | --file | -b | --blocking)
    if [ -f $2 ]; then
      code=$(cat $2)
    else
      echo "File does not exist!"
      exit 1
    fi
    ;;
  -c | --command)
    code=$2
    ;;
  *)
    echo "Usage: $0"
    exit 1
esac

ret=$(dbus-send --session --print-reply --type=method_call --dest=org.gnome.Shell /org/gnome/Shell org.gnome.Shell.Eval string:"$code")
grep -q 'boolean true' <<< "$ret"
if [ $? -eq 1 ]; then
  line="$(grep 'string' <<< $ret)"
  error=${line#"   string "}

  echo "Error executing JS: $error"
  exit 1
fi

if [ "$blockpid" != "0" ]; then
  wait $blockpid
  blockpid=0
fi

exit 0

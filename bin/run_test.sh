#!/bin/sh

SCRIPT_DIR=$(dirname $(readlink -f ${0}))

TOOLS_DIR=$SCRIPT_DIR
SEQUENCES_DIR=/home/tester/testing/sequences/
ROOT_CMD="pkexec"

export PATH=$PATH:$TOOLS_DIR
export SEQUENCES_DIR
export ROOT_CMD

if [ "$DRY_RUN" != "" ]; then
  export DRY_RUN
fi

if [ "$DESC" != "" ]; then
  export DESC
fi

if [ "$BRANCH" != "" ]; then
  export BRANCH
else
  echo "BRANCH is not defined, exiting.."
  exit 1
fi

TEST_NAME="$(basename $1)"
export TEST_NAME

if [ "$2" != "" ]; then
  remaining_runs=$2
else
  remaining_runs=1
fi

while [ $remaining_runs -gt 0 ]; do
  /bin/sh $1
  if [ $? -ne 0 ]; then
    echo "Executing test failed, exiting..."
    exit 1
  fi
  remaining_runs=$(expr $remaining_runs - 1)
done

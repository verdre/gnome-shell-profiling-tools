#!/bin/sh
# Just a small wrapper around libinput to autostart playing a recording.
# Pass "-w" or "--warp-pointer" as first argument to warp the pointer.

SCRIPT_DIR=$(dirname $(readlink -f ${0}))

if [ "$ROOT_CMD" == "" ]; then
  ROOT_CMD="pkexec"
fi

case "$1" in
  -w | --warp-pointer)
    $SCRIPT_DIR/evaljs.sh -c "Meta.get_backend().warp_pointer(global.stage.width - 1, global.stage.height - 1)"
    if [ $? -ne 0 ]; then
      echo "Warping the pointer failed, exiting..."
      exit 1
    fi

    shift
    ;;
esac

current_dir=$(pwd)
execute_root="cd $current_dir && /usr/libexec/libinput/libinput-replay --non-blocking --autoplay $@"

$ROOT_CMD /bin/sh -c "$execute_root"

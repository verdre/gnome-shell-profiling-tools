#!/bin/python3

import os
import argparse
import json
import subprocess
import plotly
import plotly.graph_objects as go
import pandas as pd

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

EXTRACT_JSON_PATH = os.path.join(SCRIPT_DIR, 'extract-data')
CAPTURE_DIR = "/home/tester/testing/out/"
PLOT_COLORS = ['mediumseagreen', 'khaki', 'lightpink', 'steelblue']

def parseMetadata(file):
    nameParts = file.replace('.syscap', '').split('_')

    if (len(nameParts) < 4 or len(nameParts) > 5):
        print("Invalid filename, skipping file: " + file)
        return {}

    fileMetadata = {
        'codeBranch': nameParts[-4],
        'testName': nameParts[-3],
        'date': nameParts[-2],
        'time': nameParts[-1],
        'fileName': file
    }

    if (len(nameParts) == 5):
        fileMetadata['userDescription'] = nameParts[-5]

    return fileMetadata


def printSortedData(df, markName):
    df = df[['testName', 'userDescription', 'codeBranch',
             'count', 'min', 'avg', 'max']]
    df = df.sort_values(by = ['testName', 'userDescription', 'codeBranch'])

    print(markName)
    print(df.to_string(index = False))
    print()


def buildBarForBranch(df, branchName, color, legendGroup, showLegend):
    # Transpose the min, avg and max columns so we can use
    # the indices as x axis and values as y axis.
    df = df[['min', 'avg', 'max']].transpose()
    df.columns = [0]
    return go.Bar(x = df.index, y = df[0],
                  marker_color = color,
                  name = branchName,
                  legendgroup = legendGroup,
                  showlegend = showLegend)


def buildAndShowCharts(df, markName):
    # Group data by user description, test name and code branch, then calculate
    # the mean value of the remaining columns (count, min, avg, max).
    df = df.groupby(['userDescription', 'testName', 'codeBranch']).mean()
    df.reset_index(inplace = True)

    userDescriptionList = df.userDescription[df.userDescription != '']
    userDescriptionList = userDescriptionList.drop_duplicates().tolist()

    noDescTestNameList = df.testName[df.userDescription == '']
    noDescTestNameList = noDescTestNameList.drop_duplicates().tolist()

    # Build a list of titles for all the plots we want to show, if the test has a
    # description, use "description (test name)", otherwise just use the test name.
    subplotTitles = []
    for desc in userDescriptionList:
        testName = df.testName[df.userDescription == desc].values[0]
        title = desc + " (" + testName + ")"
        subplotTitles.append(title)

    subplotTitles = subplotTitles + noDescTestNameList

    fig = plotly.subplots.make_subplots(rows = 1,
                                        cols = len(subplotTitles),
                                        shared_xaxes = False,
                                        shared_yaxes = True,
                                        subplot_titles = subplotTitles)

    # Build a list of data frames that we display as separate charts based
    # on the different test descriptions or (as fallback) test names.
    groupedDfs = []
    for desc in userDescriptionList:
        groupedDfs.append(df[df.userDescription == desc])
    for testName in noDescTestNameList:
        groupedDfs.append(df[df.testName == testName])

    handledBranchNames = []

    for i, groupedDf in enumerate(groupedDfs):
        colorIter = iter(PLOT_COLORS)
        countString = "Mark count (avg): "

        for j, branchName in enumerate(groupedDf['codeBranch'].drop_duplicates()):
            branchDf = groupedDf[groupedDf.codeBranch == branchName]
            color = next(colorIter)
            showLegend = False

            if (branchName not in handledBranchNames):
                handledBranchNames.append(branchName)
                showLegend = True

            bar = buildBarForBranch(branchDf, branchName, color, str(j), showLegend)
            fig.append_trace(bar, 1, i + 1)

            countString += branchName + ": " + str(branchDf['count'].values[0]) + ", "

        fig.update_xaxes(title_text = countString[:-2], row = 1, col = i + 1)

    fig.update_yaxes(title_text = 'time (ms)', row = 1, col = 1)
    fig.update_layout(title_text = markName, plot_bgcolor = 'white')
    fig.show()


parser = argparse.ArgumentParser(description = 'Extract profiling data and create charts')

parser.add_argument('-m', '--mark', nargs = '+', metavar = 'NAME',
                    help = 'The marks to analyze, by default all marks are analyzed')

parser.add_argument('-p', '--path', default = CAPTURE_DIR,
                    help = 'The folder with the sysprof capture files in it')

parser.add_argument('-r', '--raw-data', action = 'store_true',
                    help = 'Print raw profiling data to stdout and exit')

args = parser.parse_args()


allCapturesData = []
capturesDf = pd.DataFrame()
captureMarksDf = pd.DataFrame()


syscapFiles = [file for file in os.listdir(args.path) if file.endswith('.syscap')]
for file in syscapFiles:
    result = subprocess.run([EXTRACT_JSON_PATH, os.path.join(args.path, file)],
                            stdout = subprocess.PIPE)
    resultDict = parseMetadata(file)
    resultDict['captureMarks'] = json.loads(result.stdout)
    allCapturesData.append(resultDict)


# Build two data frames from our data: One data frame (capturesDf) with the
# metadata of the test, another one (captureMarksDf) with the data extracted
# from the syscap file. We connect those two data frames using the captureIndex
# column of captureMarksDf and merge them using an inner join afterwards.
for capture in allCapturesData:
    captureMarks = capture.pop('captureMarks')

    df = pd.DataFrame(data = capture, index = [0])
    if ('userDescription' not in df):
        df['userDescription'] = ""

    # Set sort = False, otherwise we get a warning
    capturesDf = capturesDf.append(df, ignore_index = True, sort = False)

    df = pd.DataFrame(data = captureMarks).transpose()
    df['markName'] = df.index
    df['captureIndex'] = capturesDf.index.stop - 1

    captureMarksDf = captureMarksDf.append(df, ignore_index = True, sort = False)

mergedDf = captureMarksDf.merge(capturesDf, left_on = 'captureIndex',
                                right_index = True, how = 'inner')
mergedDf = mergedDf.drop(columns = ['captureIndex'])

markList = args.mark or mergedDf.markName.drop_duplicates().tolist()
for markName in markList:
    markDf = mergedDf[mergedDf.markName == markName]
    if (len(markDf) == 0):
        print("Mark \"" + markName + "\" does not exist, skipping...")
        continue

    if (args.raw_data):
        printSortedData(markDf, markName)
    else:
        buildAndShowCharts(markDf, markName)

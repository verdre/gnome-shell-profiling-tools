#!/bin/sh
# Sets up an environment for testing, this will only work on newer intel
# devices with the intel_pstate driver.

CPU_PCT_LOCK=50

if [ "$ROOT_CMD" == "" ]; then
  ROOT_CMD="pkexec"
fi

if [ $EUID -ne 0 ]; then
  $ROOT_CMD $(readlink -f $0)
  exit
fi

if [ "$(cat /sys/devices/system/cpu/intel_pstate/no_turbo)" != "1" -o \
     "$(cat /sys/devices/system/cpu/intel_pstate/min_perf_pct)" != "$CPU_PCT_LOCK" -o \
     "$(cat /sys/devices/system/cpu/intel_pstate/max_perf_pct)" != "$CPU_PCT_LOCK" ]; then
  # Disable turbo boost
  echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo

  # Force the cpu to set speed
  echo $CPU_PCT_LOCK > /sys/devices/system/cpu/intel_pstate/max_perf_pct
  echo $CPU_PCT_LOCK > /sys/devices/system/cpu/intel_pstate/min_perf_pct

  exit 0
fi

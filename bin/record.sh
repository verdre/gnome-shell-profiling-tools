#!/bin/sh
# Basically a wrapper around libinput record that always passes --show-keycodes.
# Pass "-w" or "--warp-pointer" as first argument to warp the pointer.

SCRIPT_DIR=$(dirname $(readlink -f ${0}))

ROOT_CMD="pkexec"

case "$1" in
  -w | --warp-pointer)
    $SCRIPT_DIR/evaljs.sh -c "Meta.get_backend().warp_pointer(global.stage.width - 1, global.stage.height - 1)"
    if [ $? -ne 0 ]; then
      echo "Warping the pointer failed, exiting..."
      exit 1
    fi

    shift
    ;;
esac

finish () {
  if [ ! -f $1 ]; then
    exit 1
  fi

  uid=$(id -u $USER)
  gid=$(id -g $USER)

  execute_root="cd ${current_dir} && chown ${uid}:${gid} ${output_file}"
  $ROOT_CMD /bin/sh -c "$execute_root"

  tail -n8 $1 | head -n4 | grep -q "KEY_LEFTCTRL"
  key_ctrl_found=$?
  tail -n4 $1 | grep -q "KEY_C"
  key_c_found=$?

  if [ $key_ctrl_found -eq 0 -a $key_c_found -eq 0 ]; then
    echo ""
    echo "Found ctrl-c sequence at the end, you might want to remove it"
  fi
}

for arg in $@; do
  if [ "$found" == "1" ]; then
    output_file=$arg
    break
  fi
  if [ "$arg" == "-o" ]; then
    found=1
  fi
done

trap 'finish $output_file' EXIT

current_dir=$(pwd)

execute_root="cd ${current_dir} && libinput record --show-keycodes $@"
$ROOT_CMD /bin/sh -c "$execute_root"

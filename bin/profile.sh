#!/bin/sh
# Sets up an environment for testing and does the supplied test
# Setting up the environment only works on newer intel devices with intel_pstate

SCRIPT_DIR=$(dirname $(readlink -f ${0}))

OUT_DIR=/home/tester/testing/out/
ROOT_CMD="pkexec"

mkdir -p $OUT_DIR

date="$(date +%Y-%m-%d_%H:%M:%S)"

filename="${BRANCH}_${TEST_NAME}_${date}"
if [ "$DESC" != "" ]; then
  filename="${DESC}_${filename}"
fi

$SCRIPT_DIR/setup_env.sh
if [ $? -ne 0 ]; then
  echo "Setting up environment failed, exiting..."
  exit 1
fi

cmd=$(command -v "$1")
if [ $? -ne 0 -a ! -x "$cmd" ]; then
  echo "No or wrong command supplied to sysprof, exiting..."
  exit 1
fi

if [ "$DRY_RUN" == "true" ]; then
  $($@)
  exit 0
fi

sysprof-cli -c "$*" --gjs --gnome-shell "${OUT_DIR}/${filename}.syscap"
if [ $? -ne 0 ]; then
  echo "Sysprof failed, removing recording"
  rm "${OUT_DIR}/${filename}.syscap"
  exit 1
fi

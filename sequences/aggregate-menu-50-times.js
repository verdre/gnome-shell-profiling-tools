// This test opens the app grid 5 times. It behaves like pressing CMD+A 10 times.
let GLib = imports.gi.GLib;

let count = 0;

GLib.timeout_add(GLib.PRIORITY_DEFAULT, 100, () => {
    Main.panel.statusArea.aggregateMenu.menu.toggle()

    count++;
    if (count == 50) {
        log("TEST_FINISHED");
        return GLib.SOURCE_REMOVE;
    }

    return GLib.SOURCE_CONTINUE;
});


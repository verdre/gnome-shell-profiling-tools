// This test opens the app grid 5 times. It behaves like pressing CMD+A 10 times.
let GLib = imports.gi.GLib;

let count = 0;

GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 1, () => {
    //Main.overview.viewSelector._toggleAppsPage();

    count++;
    if (count == 4) {
        log("TEST_FINISHED");
        return GLib.SOURCE_REMOVE;
    }

    return GLib.SOURCE_CONTINUE;
});

